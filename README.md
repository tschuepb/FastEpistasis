***FastEpistasis***, a software tool capable of computing tests of epistasis for a large number of SNP pairs, is an efficient parallel extension to the PLINK epistasis module. It tests epistatic effects in the normal linear regression of a quantitative response on marginal effects of each SNP and an interaction effect of the SNP pair, where SNPs are coded as additive effects, taking user defined values or the default 0, 1 and 2. The test for epistasis reduces to testing whether the interaction term is significantly different from zero.

FastEpistasis optimizes the computations by splitting the analysis tasks into three separate applications: pre-, core- and post-computation. 
* The *precomputation* phase loads PLINK binary format data files, reformats the data for faster computations and reduces the number of conditions to test for in the core phase. 
* The *core computational* phase is designed to embarrassingly parallelize the computations, iterating through SNP pairs and efficiently carrying out the tests for epistasis. The computations are based on applying the QR decomposition to derive least squares estimates of the interaction coefficient and its standard error. The core computation software comes in several versions to take advantage of different high performance architectures - a Shared Memory Processor (SMP) version and a clustered Message Passing Interface (MPI) version. 
* An optional *post*-computation phase is provided to aggregate results from each processor or core, include detailed SNP information, compute p-values from each test, and convert to text files. 

As of version 2.03, FastEpistasis will also test epistatic effects for binary traits using logistic regression model similar to PLINK.
As of version 2.05, preFastEpistasis automatocally uses all to all snps interactions when no set file is provided. Furthermore, genetic data can now be stored in 2bit format such as in Plink, rather than 1 byte. This is triggered by at compile time. We now provide two tools: one to test performance of the QR algorithm based upon the chosen method and another to have quick access to a snp A -snp B epistatic interaction results given snps A and B. 

## Source code
Latest enhancements and features of FastEpistasis (version 3) are now available as branches. There are version v1, v2 and v3.
Version 3 is still considered beta, but you are welcome to use it and report bugs.  

## Download
[FastEpistasis-2.07.tar.bz2](/uploads/864695cfa08720b46dd1b437faca3fff/FastEpistasis-2.07.tar.bz2)
## Reference
***FastEpistasis: A high performance computing solution for quantitative trait epistasis.*** Schuepbach T, Xenarios I, Bergmann S, Kapur K . Bioinformatics. 2010 Apr 7 